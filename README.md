# Application configs
This sets up the programs that I use and their settings.

<br/>

## Table of contents <a name="TOC"></a>
- [Installation](#repo_Install)
- [Install applications](#app_Install)
- [Install flatpaks](#flatpak_Install)
- [Change default applications](#Defaults)
- [Configure firewall](#firewall)
- [Ranger config](#Ranger_config)
- [Install python programs](#python)
- [Firefox config](#Firefox_config)
- [Brave config](#Brave_config)
- [Geany config](#Geany_config)
- [Redshift config](#Redshift_config)
- [Add a printer](#printer)
- [Bluetooth setup](#bluetooth)
- [Clean-up](#Cleanup)
- [Licence and Status etc.](#End)


<br/><br/>


## [Installation of this repository](#TOC) <a name="repo_Install"></a>
### Download
```
git clone https://gitlab.com/zyxomma1/application-configs
```
### Increase parallel downloads for pacman
This will allow more packages to be downloaded by pacman at once. This is **only for Arch based systems.**
```
sudo sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 6/' /etc/pacman.conf
```


<br/><br/>


## [Install applications](#TOC) <a name="app_Install"></a>
[Acknowledgement](https://wiki.archlinux.org/title/Pacman/Tips_and_tricks#List_of_installed_packages)[^1]
Install the listed applications and remove unwanted packages. This **only works for Arch and Debian.**
```
cd "/home/$USER/application-configs/"			#cd into downloaded git repo
chmod +x install_packages				#make shell script executable
./install_packages					 #run script that Installs applications I like
```


<br/><br/>


## [Install flatpak](#TOC) <a name="flatpak_Install"></a>
Flatpak is a containerised (system independent) way to package software. This allows a stable release system to run the latest release of a program in isolation from the rest of the system. 
You can install `flatpak` from your package manager. Use the following code to add the flathub repository.
```
flatpak remote-add --if-not-exists flathub https://dl.flathub.org/repo/flathub.flatpakrepo
```
Your system needs to reboot before flatpak can be used
```
sudo shutdown --reboot now
```
You can now install and search using flatpak
```
flatpak install czkawka nomacs
```
Manage Flatpak permissions and file storage
```
flatpak install flatseal
```
List apps available in a specific repository
```
flatpak remote-ls flathub --app
```


<br/><br/>


## [Change default applications](#TOC) <a name="Defaults"></a>
```
cd ~ 	#replace mimeapps
mv "/home/$USER/application-configs/git_mimeapps.list" "/home/$USER/.config/mimeapps.list"
```


<br/><br/>


## [Configure firewall](#TOC) <a name="firewall"></a>
These commands enable the firewall but allow ssh connections using `ufw` 
```
sudo ufw enable			#enable
sudo ufw status verbose		#check its status
sudo ufw allow ssh		#open the ssh port
```
To view what applications ufw controls run the following command
```
sudo ufw app list		#list controllable application connections
```


<br/><br/>



## [Ranger config](#TOC) <a name="Ranger_config"></a>
```
ranger				#open ranger
ranger --copy-config=all	#creates config files in /home/$USER/.config/ranger/
line_old='show_hidden false'	#replaces line in config file
line_new='show_hidden true'
sed -i "s%$line_old%$line_new%g" "/home/$USER/.config/ranger/rc.conf"	# edit rc.conf to show hidden files
```


<br/><br/>


## [Install python programs](#TOC) <a name="python"></a>
[Gallery-dl documentation](https://github.com/mikf/gallery-dl#readme)[^2][yt-dlp documentation](https://github.com/yt-dlp/yt-dlp#readme)
Install gallery-dl and yt-dlp. **They are in the install list for arch.**
### Install without virtual environment 
Install the image scraper `gallery-dl` and the video downloader `yt-dlp` without sandboxing in a virtual environment (break-system-packages). **Note** that '$HOME/.local/bin' needs to be added to the path for these programs to run.
```
python3 -m pip install requests --break-system-packages			#install gallery-dl dependencies
python3 -m pip install -U gallery-dl --break-system-packages	#install gallery-dl
python3 -m pip install -U yt-dlp --break-system-packages		#install youtube-dlp

```
### Install with virtual environment
[Video tutorial](https://youtu.be/Kg1Yvry_Ydk?si=TRUGgcvXO0LbI2Xp)
To install within a virtual environments create a folder to contain them.To create environments successfully $\textcolor{red}{\text{python3.11-venv}}$ needs to be installed.
```
cd $HOME/.config/
mkdir python && cd python	#create python folder
```
Copy the following to create a virtual enviroment for `gallery-dl`.
```
cd $HOME/.config/python
python3 -m venv gallery					#create virtual environment for gallery-dl
source gallery/bin/activate				#activate virtual environment
which python 							#make sure it is activated
pip list								#list packages available in folder
python3 -m pip install -U gallery-dl	#install gallery-dl
deactivate								#exit virtual enviroment
```
Copy the following to create a virtual environment for `yt-dlp`.
```
python3 -m venv yt					#create virtual environment for yt-dlp
source yt/bin/activate				#activate virtual environment
which python 						#make sure it is activated
pip list							#list packages available in folder
python3 -m pip install -U yt-dlp	#install yt-dlp
deactivate							#exit virtual enviroment
```
**Note** that these need to be added to the path for these programs to run.
```
export PATH="$PATH:$HOME/.config/python/yt:$HOME/.config/python/gallery"
```
For global use of these programs it may be useful to add alias'.
```
alias yt-dlp="$HOME/.config/python/yt/bin/yt-dlp"
alias gallery-dl="$HOME/.config/python/gallery/bin/gallery-dl"
```


<br/><br/>


## [Firefox config](#TOC) <a name="Firefox_config"></a>
### Open following links with firefox-esr
```
alias firefox='firefox-esr'
```
### Install Add-ons 
```
firefox about:addons
firefox https://addons.mozilla.org/en-GB/firefox/addon/cookies-txt/		#Cookies.txt
firefox	https://addons.mozilla.org/en-GB/firefox/addon/ublock-origin/		#uBlock Origin
firefox https://addons.mozilla.org/en-GB/firefox/addon/darkreader/		#Dark Reader
firefox https://addons.mozilla.org/en-GB/firefox/addon/video-downloadhelper/	#Video DownloadHelper
```
### Install Video DownloadHelper companion app
The new downloader is found in [Github](https://github.com/aclap-dev/video-downloadhelper/wiki/CoApp-Installation) and [their website.](https://www.downloadhelper.net/install-coapp?browser=firefox)

This will download and install the companion app for any distro.
```
cd "$HOME/Downloads"
curl -sSLf https://github.com/aclap-dev/vdhcoapp/releases/latest/download/install.sh | bash
rm install.sh		#clean up directory
```
The following is an auto downloader I wrote before they created the above code. 
```
cd "$HOME/Downloads"
latest_release=$(curl -s "https://api.github.com/repos/aclap-dev/vdhcoapp/releases/latest")	#Get the latest release information
tag_name=$(echo "$latest_release" | grep -o '"tag_name": ".*"' | cut -d'"' -f4 | cut -c 2-)
if command -v nala &> /dev/null; then									#checks if package manager exists then downloads file and installs program 
	echo nala
	wget https://github.com/aclap-dev/vdhcoapp/releases/download/v${tag_name}/net.downloadhelper.coapp-${tag_name}-1_amd64.deb 
	sudo dpkg -i net.downloadhelper.coapp-${tag_name}-1_amd64.deb
	sudo nala install --fix-broken										#to fix dependencies
	rm net.downloadhelper.coapp-${tag_name}-1_amd64.deb					#remove deb package
else
	echo other
	wget https://github.com/aclap-dev/vdhcoapp/releases/download/v${tag_name}/net.downloadhelper.coapp-${tag_name}-1_amd64.tar.gz
	sudo tar -zxvf net.downloadhelper.coapp-${tag_name}-1_amd64.tar.gz -C ~						#extract tar file
	~/net.downloadhelper.coapp-${tag_name}/bin/net.downloadhelper.coapp-linux-64 install --user	#install in home directory
	rm net.downloadhelper.coapp-${tag_name}-1_amd64.tar.gz										#remove download
fi
```
### Change toolbar 
- Right click on toolbar and click customise toolbar
	- untick Title Bar
- Right click on toolbar
	- change bookmarks to only on new tab
- Remove pocket from toolbar
- Remove Firefox account
- Add sidebars
- Add home and set to file
- Remove spaces
### Custom Firefox css 
[Tutorial by Matthew Webber](https://youtu.be/aCdFs6rgeNI)[^3]
```
firefox about:config
```
- search for	`toolkit.legacy`
- change to		`true`
```
cd ~
cd .mozilla/firefox/
default_release="$(ls | grep 'default-')"	#finds default-release or default-esr folder as it's name different for every system
cd "$default_release"
mkdir chrome						#make chrome folder in 	.mozilla/firefox/#######.default-release/ or .mozilla/firefox/#######.default-esr/
cd chrome
cd ~
mv "/home/$USER/application-configs/Firefox_userChrome.css" "/home/$USER/.mozilla/firefox/$default_release/chrome/userChrome.css"
```
### Make search bar condensed 
```
firefox about:config
```
- search for	`uidensity`
- change to 	`1`
### Change settings
```
firefox about:preferences#general
 ```
| Item to change | Setting |
|---|---|
| Autoscrolling | enable |
```
firefox about:preferences#home
```
Change startpage by going to `homepage and new windows` and pasting where the customised startpage file is located.
| Item to change | Setting |
|---|---|
| pocket and sponsored shortcuts | off |
```
firefox about:preferences#privacy
```
| Item to change | Setting |
|---|---|
| Tracking | Standard |
| Do Not Track | Always |
| location, camera, microphone | Block |  
| Data collection | off |  
| HTTPS-only mode | on |

### Import bookmarks 
- press ctrl+shift+o 
- click import and backup
	- Using json file:
		- Restore > choose file
	- Using HTML file:
		- Import bookmarks from HTML > choose file
### Import passwords
To allow for the import of CSV files the import button must be enabled. Disable it after import as it is hidden for performance reasons.
```
firefox about:config
```
- search for	`signon.management.page.fileImport.enabled`
- change to		`true`

```
firefox about:logins
```
- click on three dots
- import from a file


<br/><br/>


## [Brave config](#TOC)	<a name="Brave_config"></a>
Brave is not in all repositories by default. View [their website](https://brave.com/linux/#release-channel-installation) to see how to download it for your distribution.
### Open following links with brave-browser-stable
```
alias brave='brave-browser-stable'
```
### Install Add-on 
```
#Dark Reader
brave https://chrome.google.com/webstore/detail/dark-reader/eimadpbcbfnmbkopoojfekhnkhdbieeh?hl=en-GB
```
### Change startpage
```
brave brave://settings/getStarted
```
select `On Start-up` and paste where customised startpage file is located
### Change appearance
```
brave brave://settings/appearance
```
| Item to change | Setting |
|---|---|
| Theme |														Use Classic |
| Show Home button | 											on |
| Enter custom web address | 									Paste where customised startpage file is located |
| Show bookmarks bar | 											Never |
| Use wide address bar | 										on |
| Show autocomplete in address bar | 							on |
| Show top sites in autocomplete suggestions | 					off |
| Show Brave suggested sites in autocomplete suggestions |  	off |
| Always show full URLs	| 										off |
| Show tab search button | 										off |
| Use system title bar and borders | 							off |
| Cycle through the most recently used tabs with Ctrl-Tab | 	on |
### Change homepage
```
brave brave://settings/newTab
```
Set `New tab page shows` to `Homepage`
### Change Shield appearance
```
brave brave://settings/shields
```
| Item to change | Setting |
|---|---|
| Show the number of blocked items on the Shields icon | 		off |
### Hide Brave rewards
```
brave brave://settings/rewards
```
| Item to change | Setting |
|---|---|
| Show Brave rewards icon in address bar | 							off |
### Add to sync chain
```
brave brave://settings/braveSync
```
### Hide Brave wallet
```
brave brave://settings/web3
```
| Item to change | Setting |
|---|---|
| Show Brave wallet icon on toolbar | 							off |


<br/><br/>


## [Geany config](#TOC) <a name="Geany_config"></a>
[Tutorial by Derek Taylor](https://www.youtube.com/watch?v=N-ZXBt2cETs)[^4]
[Geany themes documentation](https://github.com/codebrainz/geany-themes#readme)[^5]
```
sudo pacman -Syu geany geany-plugins
git clone https://github.com/codebrainz/geany-themes.git
cd geany-themes
./install.sh
cd ..
rm -rf geany-themes
```
- Edit
	- Preferences
		- **General**
			- *Startup*
				- Load files from last session off
			- *Miscellaneous*
				- Always wrap search
		- **Interface**
			- *Toolbar*
				- Append toolbar to menu
				- Very small icons
		- **Editor**
			- *Features*
				- Line wrapping on
				- Newline strips trailing spaces on
			- *Completions*
				- Auto-close all brackets
			- *Display*
				- Show markers margin off
				- Long line marker disable
		- **Tools**
			- Browser brave 
		- **Terminal** 
			- Background colour #262626

- Tools
	- Plugin manager
		- **TreeBrowser**
		- **Overview**
		- **Split** make shortcuts side by side ctrl+shift+left  top and bottom ctrl+shift+up  unsplit ctrl+shift+down

- Edit
	- Plugins preferences
		- **Overview**
			- Hide editor scroll bar on
			- Width 100
		- **Tree browser**
			- Show hidden files on
			- Focus editor on file open on

- View
	- Change colour scheme
		- **Darkula**


<br/><br/>


## [Redshift config](#TOC) <a name="Redshift_config"></a>
Redshift changes the colour of your screen so that less bluelight is emmited later in the day. The config can be coppied over using the following comand
```
cd "/home/$USER/application-configs/"			#cd into downloaded git repo
mv redshift.conf "$HOME/.config/redshift.conf"
```


<br/><br/>


### [Add a printer](#TOC) <a name="printer"></a>
Install cups, start it and add the user to the printing group
```
if command -v nala &> /dev/null; then									#checks if package manager exists then downloads prerequisites with said package manager
	printf "${red_colour_code} Ubuntu/Mint/Debian distros \033[0m\n"
	printf "${cyan_colour_code} Install cups \033[0m\n"
	sudo nala install cups
elif command -v pacman &> /dev/null; then
	printf "${red_colour_code} ArchLinux \033[0m\n"
	printf "${cyan_colour_code} Install cups \033[0m\n"
	sudo pacman -S cups
fi

sudo systemctl enable --now cups		#start cups

# Check if the user is part of the 'lp' group
if id -nG "$USER" | grep -qw "lp"; then
    echo "User $USER is already part of the 'lp' group."
else
    echo "User $USER is not part of the 'lp' group. Adding now..."
    
    # Add the user to the 'lp' group
    sudo usermod -aG lp "$USER"

    # Confirm the addition
    if id -nG "$USER" | grep -qw "lp"; then
        echo "User $USER has been successfully added to the 'lp' group."
    else
        echo "Failed to add user $USER to the 'lp' group. Please check permissions."
    fi
fi
```
Add there printer by going to `http://localhost:631/admin` in the browser


<br/><br/>


### [Bluetooth setup](#TOC) <a name="bluetooth"></a>
Install bluetooth gui frontend and libraries
```
if command -v nala &> /dev/null; then									#checks if package manager exists then downloads prerequisites with said package manager
	printf "${red_colour_code} Ubuntu/Mint/Debian distros \033[0m\n"
	printf "${cyan_colour_code} Install cups \033[0m\n"
	sudo nala install blueman libspa-0.2-bluetooth
elif command -v pacman &> /dev/null; then
	printf "${red_colour_code} ArchLinux \033[0m\n"
	printf "${cyan_colour_code} Install cups \033[0m\n"
	sudo pacman -S blueman libspa-0.2-bluetooth
fi

sudo shutdown --reboot now
```
[Connect via terminal](https://www.baeldung.com/linux/bluetooth-via-terminal)
<br/>
The following shows what devices are available.
```
bluetoothctl power on
bluetoothctl discoverable on
bluetoothctl pairable on
bluetoothctl list
bluetoothctl scan on & sleep 5 && bg
bluetoothctl devices
```
To connect copy the mac address of the device you want to connect into the following commands.
```
bluetoothctl pair #mac
bluetoothctl connect #mac
```
#### Add audio auto switching
The following code will automatically switch the audio output device to the connected device 
```
# Define the lines to add
lines_to_add="### Automatically switch to newly-connected devices
load-module module-switch-on-connect"

# Target file
target_file="/etc/pulse/default.pa"

# Check if the lines are already present
if grep -Fxq "load-module module-switch-on-connect" "$target_file"; then
    echo "The configuration is already present in $target_file. No changes made."
else
    # Append the lines to the file
    echo "$lines_to_add" | sudo tee -a "$target_file" > /dev/null
    echo "Configuration added successfully to $target_file."
fi

#restart pulse audio
pulseaudio -k
pulseaudio --start
```


<br/><br/>


### [Clean-up](#TOC) <a name="Cleanup"></a>
```
cd ~
rm -rf application-configs			#delete the git folder
```


<br/><br/>


<a name="End"></a>
## Contributing 
No contributions will be accepted because this is how I like to setup my computer. Feel free to fork it if you want.

## License
The files and scripts in this repository are licensed under the MIT License, which is a very permissive license allowing you to use, modify, copy, distribute, sell, give away, etc. the software. In other words, do what you want with it. The only requirement with the MIT License is that the license and copyright notice must be provided with the software.

## Project status
I just add stuff when it needs backed up.

## Authors and acknowledgement
0.  Me (Obviously)
[^1]: [Arch Linux documentation](https://wiki.archlinux.org)
[^2]: [Mike Fährmann](https://github.com/mikf)
[^3]: [Matthew Webber](https://www.youtube.com/@TheLinuxCast)
[^4]: [Derek Taylor](https://distro.tube/)
[^5]: [Matthew Brush](https://github.com/codebrainz)
